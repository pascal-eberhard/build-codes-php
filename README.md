# File info tool

Get relevant infos about files and folders. Configurable and expandable

For file archive and backup strategies, it would be good to have some data about the files. Which properties have the file, filter duplicate files etc.

Sure there are tools for that, but with some limitations. Some are not free, some are not available for the needed operation system and all the tools output in different data formats.

I want to ..
* Build a tool which can do all this, surely using existing libraries and project for the most parts.
* Make it configurable via a config file.
* Make it expandable.

The best base for this in PHP would be the symfony micro kernel and matching symfony components.

So let's start.

.... TODO rework part below ....

Pack as .phar, see [https://github.com/sensiolabs/security-checker].
