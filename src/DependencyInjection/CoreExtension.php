<?php
namespace App\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

class CoreExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
//        throw new \LogicException('x');
//        echo \PHP_EOL;
//        \var_export([
//            '$configs' => $configs,
//            '$this->getAlias()' => $this->getAlias(),
//        ]);
//        echo \PHP_EOL;
//        exit;
//        // ... you'll load the files here later
    }

    public function getAlias()
    {
        return 'fileInfo';
    }
}
