<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RunCommand extends Command
{
    /**
     * The command name.
     *
     * @var string
     */
    const COMMAND = 'fileInfo:run';

    /**
     * The command description.
     *
     * @var string
     */
    const DESCRIPTION = 'Get relevant infos about files and folders. Configurable and expandable';

    /**
     * The tool.
     *
     * @var null
     */
    private $fileInfo = null;

    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setDescription(self::DESCRIPTION)
            ->setName(self::COMMAND)

            ->addOption('config', null, InputOption::VALUE_OPTIONAL, 'Relative path to config file')
            ->addOption('path', null, InputOption::VALUE_OPTIONAL, 'The start folder path')
        ;
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '<info>File info tool</info>',
            '<comment>'.self::DESCRIPTION.'</comment>',
        ]);
//        $output
//        if ($endPoint = $input->getOption('end-point')) {
//            $this->checker->getCrawler()->setEndPoint($endPoint);
//        }
//
//        if ($timeout = $input->getOption('timeout')) {
//            $this->checker->getCrawler()->setTimeout($timeout);
//        }
//
//        if ($token = $input->getOption('token')) {
//            $this->checker->getCrawler()->setToken($token);
//        }
//
//        $format = $input->getOption('format');
//        if ($input->getOption('no-ansi') && 'ansi' === $format) {
//            $format = 'text';
//        }
//
//        try {
//            $result = $this->checker->check($input->getArgument('lockfile'), $format);
//        } catch (ExceptionInterface $e) {
//            $output->writeln($this->getHelperSet()->get('formatter')->formatBlock($e->getMessage(), 'error', true));
//
//            return 1;
//        }
//
//        $output->writeln((string) $result);
//
//        if (\count($result) > 0) {
//            return 1;
//        }

        return 0;
    }
}
